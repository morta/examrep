<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class DealupdateController extends Controller
{
	public function actionAddrule()
	{
		$auth = Yii::$app->authManager;				
		$updateOwnDeal = $auth->getPermission('updateOwnDeal');
		$auth->remove($updateOwnDeal);
		
		$rule = new \app\rbac\OwnDealRule;
		$auth->add($rule);
				
		$updateOwnDeal->ruleName = $rule->name;		
		$auth->add($updateOwnDeal);	
	}
}
