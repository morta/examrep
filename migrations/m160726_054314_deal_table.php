<?php

use yii\db\Migration;

class m160726_054314_deal_table extends Migration
{
    public function up()
    {
		$this->createTable(
		'deal',
			[
				'Id' => 'pk',
				'leadId' => 'INT',
				'name' => 'string',
				'amount' => 'INT'
			]
		);
    }

    public function down()
    {
	$this->dropTable('deal');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
